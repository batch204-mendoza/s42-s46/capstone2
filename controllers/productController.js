const Products = require("../models/Products");



// Create a New Product

module.exports.addProduct = (reqBody) => {

	let newProduct = new Products({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return Products.find({name: reqBody.name}).then(result =>{

		if (result.length > 0) {
			console.log(`product name "${reqBody.name}" is already in use pick another name`)
			return false		}
		else{ 
			return newProduct.save().then( (product, error) => {
			if(error) 
			{
				return false
			} 
			else 
			{
				return true
			}
			});
		}	
  })
}

//updating porudct
module.exports.updateProduct = (reqParams,reqBody,data) =>{
	if(data.isAdmin===true){
		let updatedProduct = {
			name: reqBody.name,
			description:reqBody.description,
			price: reqBody.price
		};
	
	return Products.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error) =>{
		if (error){
			return false
		}else{
			
			return true
		}
	}).catch(error => {return "id does not exist"});
	}
	else {

		return false
	}

}


//retrieve all products
module.exports.getAllProducts= () =>{
	return Products.find({}).then(result =>{
		return result;
	})
};

// retrieving a specific product by id
//ask if theres a method to use  the url input as identifier for true false statement
module.exports.getProduct = (reqParams) =>{
	return Products.findById(reqParams.productId).then(result => {
		return result
	}).catch(error => {return false});
}




//archive course
module.exports.archiveProduct = (reqParams,reqBody,data) =>{
	if(data.isAdmin===true){
		let archivedProduct = {
			
			isActive:reqBody.isActive

		};

		
	
	return Products.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product,error) =>{
		if (error){
			
			return false
		}else{

		
			return true
		}
	}).catch(error => {return "id does not exist"});
	}
	else {

		return false
	}

}

