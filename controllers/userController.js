const User = require ("../models/User");
const bcrypt= require("bcrypt")
const auth= require("../auth")
const Products = require("../models/Products");



module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then((result, error) => {
		if(error){return false}
		if(result.length > 0) {
			return true
		} else {
			return false
		}

	})
}




module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then((result, error) => {
		if(error){return false}
		if(result === null){

			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((user, error) => {

				if(error) {
					return false
				} else {
					return true
				}
			})	
		}
		else{
			return false
		}
	})
}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then((result, error) => {

		if(error){return false}

		if(result == null) {
			
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {

				return false
			}
		}

	})
}


// //login controller
// module.exports.loginUser = (reqBody) =>{
// 	return User.findOne({email: reqBody.email}).then(result => {

// 		if (result ==null) {
// 			return false
// 		} else {
// 			const isPasswordCorrect = bcrpyt.compareSync(reqBody.password,result.password);


// 			if(isPasswordCorrect){
// 				return { access: auth.createAccessToken(result)}
// 			}else {
// 				return false
// 			}
// 		}
// 	})
// }


//check if email exists* can retire this
// module.exports.checkEmailExists = (reqBody) => {

// 	return User.find({email: reqBody.email}).then(result =>{

// 		if (result.length > 0) {
// 			return true
// 		}
// 		else{
// 			return false
// 		}
// 	})
// }


// // controller for user registration
// module.exports.registerUser = (reqBody) =>{
	
// 	console.log("test1")
// 	let newUser = new User({
// 		firstName:reqBody.firstName,
// 		lastName:reqBody.LastName,
// 		MobileNo:reqBody.mobileNo,
// 		email: reqBody.email,
// 		password: bcrpyt.hashSync(reqBody.password, 10)
// 	});
// 	console.log("test2")
// 	console.log(newUser)
// 	return User.find({email: reqBody.email}).then(result =>{

// 		if (result.length > 0) {
			
// 			console.log("error1")
// 			console.log("test3")
// 			return false
// 		}
// 		else{
// 			return newUser.save().then((user, error) =>{
// 				if (error){
// 					console.log("error1")
// 					return false
// 				}
// 				else{
// 					console.log("working")
// 					return true
// 				}
// 			})
// 		}
// 	})
	
// }

// // login controller
// module.exports.loginUser = (reqBody) =>{
// 	return User.findOne({email: reqBody.email}).then(result => {

// 		if (result ==null) {
// 			return false
// 		} else {
// 			const isPasswordCorrect = bcrpyt.compareSync(reqBody.password,result.password);


// 			if(isPasswordCorrect){
// 				return { access: auth.createAccessToken(result)}
// 			}else {
// 				return false
// 			}
// 		}
// 	})
// }




//  
module.exports.addToCart = (data)=>{

	if (data.isAdmin){

		return false
	}
	else{
	//let isUserUpdated = await // remove return on nex line and connect it to let user for await async
	return	User.findById(data.userId).then(user =>{
	

		user.orders.push({products: data.products, totalAmount:data.totalAmount})


		return user.save().then((user,error) => {

			if(error){
				return false
			}	
			else{
				return true
			}
		})
	})
}

/*	let isProductUpdated = await Products.findById(data.productId).then(product =>{
		product.orders.push({orderId: data.orderId})
		return product.save().then((product,error) =>{
			if(error){
				return false
			}
			else {
				return true
			}
		})
	})

	if (isUserUpdated && isProductUpdated){
		return true
	}else{
		return false
	}
	}*/
}




// Retrieve user details

module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "";
		return result;

	});

}; 	  	


//updating to admin
module.exports.userToAdmin = (reqParams,reqBody,data) =>{
	if(data.isAdmin===true){
		let updatedUser = {
			isAdmin:true
		};
	
	return User.findByIdAndUpdate(reqParams.userId,updatedUser).then((product,error) =>{
		if (error){
			return false
		}else{
			
			return true
		}
	}).catch(error => {return "id does not exist"});
	}
	else {

		return false
	}

}




module.exports.getOrders = (data) => {

	return User.findById(data.id).then(result => {

	
		return result.orders;

	});

}; 	  	
/*
//archive course
module.exports.archiveProduct = (reqParams,reqBody,data) =>{
	if(data.isAdmin===true){
		let archivedProduct = {
			
			isActive:reqBody.isActive

		};*/