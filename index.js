const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")
const userRoutes= require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const port = process.env.PORT || 4000;
const app= express();

mongoose.connect("mongodb+srv://Jack:admin123@cluster0.o4i73.mongodb.net/s42-s46?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", ()=> console.log("now connected to MongoDB Atlas!"))

app.use(cors());
app.use(express.json());


app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.listen(port,() =>{
	console.log(`API is now online on port ${port}`);
});



