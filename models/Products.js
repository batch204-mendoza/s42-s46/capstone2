const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number, 
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}	
		}
	]

	
});


module.exports = mongoose.model("Product", productSchema);






// /*Product
// ○ Name (String)
// ○ Description (String)
// ○ Price (Number)
// ○ isActive (Boolean - defaults to true)
// ○ createdOn (Date - defaults to current timestamp)
// ○ orders (Array of Objects)
// ■ orderId (String)*/


// const mongoose = require("mongoose");

// const productSchema = new mongoose.Schema({
// 	name:{
// 		type: String,
// 		required: [true, "course is required"]

// 	},
// 	description: {
// 		type: String,
// 		required: [true, "Description is required"]

// 	},
// 	price: {
// 		type: Number,
// 		required: [true, "Price is required"]

// 	},
// 	isActive: {
// 		type: Boolean,
// 		default: true
// 	},
// 	createdOn: {
// 		type: Date,
// 		default: new Date()
// 	}

// 	/*orders: [{
// 		orderId:{
// 			type: String,
// 	//		required: [true,"userId is required"]
// 		}
// 	}]*/

// })


// // creates a "Course" collection in the database if  no course exists yet. but will update into the system if it already exists
// module.exports = mongoose.model("Products", productSchema);