const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	orders : [
		{
			products : [
				{
					productId : {
						type : String
					},
					productName : {
						type: String
					},
					price : {
						type: Number
					},
					quantity : {
						type: Number
					},
					subtotal : {
						type: Number
					}		
				}
			],	
			totalAmount : {
				type : Number
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);






// /*
// User
// ○ Email (String)
// ○ Password (String)
// ○ isAdmin (Boolean - defaults to false)
// ○ orders (Array of Objects)
// ■ products (Array of Objects)
// ● productName (String)
// ● quantity (Number)
// ■ totalAmount (Number)
// ■ purchasedOn (Date - defaults to current timestamp
// */


// const mongoose = require("mongoose");
// const userSchema = new mongoose.Schema({
	



// 	firstName: {
// 		type: String, 
// 		required: [true, "first Name is required"]
// 	},
// 	lastName: {
// 		type: String, 
// 		required: [true, "password is required"]
// 	},
// 	MobileNo: {
// 		type: String, 
// 		required: [true, "MobileNo is required"]
// 	},
// 	email: {
// 		type: String,
// 		required: [true, "Email is required"]
// 	},
// 	password: {
// 		type: String, 
// 		required: [true, "password is required"]
// 	},
// 	isAdmin: {
// 		type: Boolean,
// 		default: false
// 	},
// 	orders: [{
// 		products: [{
// 			productName:{
// 				type: String,
// 			//	required: [true,"product name is required"]
// 			},
// 			quantity:{
// 				type: Number,
// 			//	required: [true,"quantity required"]
// 			}

// 		}],

// 		totalAmount:{
// 			type: Number,
// 		//	default: 0
// 		},
// 		purchasedOn: {
// 			type: Date,
// 			default: new Date()
// 		}

//   }]
// })
	

// module.exports = mongoose.model("User", userSchema);
