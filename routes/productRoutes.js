const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');


//Route for creating a product
router.post("/addproducts", auth.verify, (req, res) => {

		const isAdmin= auth.decode(req.headers.authorization).isAdmin;
		console.log(isAdmin)

		if(isAdmin){
			productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));

		}
		else{
			console.log("not authorized to add Product")
			return res.send(false)
		}

});

//route for updating a product
router.put("/:productId", auth.verify, (req,res) => {

	const productData= auth.decode(req.headers.authorization)
	
	productController.updateProduct(req.params, req.body, productData).then(resultFromController => res.send(resultFromController))
});


//route for retrieving all the products
router.get("/viewallproducts", (req,res)=>{

	/*const isAdmin= auth.decode(req.headers.authorization).isAdmin;
		console.log(isAdmin)

		if(isAdmin){
			productController.getAllProducts().then(resultFromController => res.send(resultFromController));
		}
		else{
			return res.send("not authorized to view all Products")
		}*/
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// route for retrieving specific product
router.get("/:productId", (req,res) =>{

	productController.getProduct(req.params).then(resultFromController=> res.send(resultFromController));
});

	

//route for Archiving a product/ unarchiving a product
router.put("/:productId/archive", auth.verify, (req,res) => {

	const archiveData= auth.decode(req.headers.authorization)
					 
	productController.archiveProduct(req.params, req.body, archiveData).then(resultFromController => res.send(resultFromController))
})

module.exports=router;
