const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");
/*
// route for checking if the urser's email already exists in our database can retire this
router.post("/checkEmail", (req,res) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// routes for user registration
router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});
*/



router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});









// customer add to cart
router.post("/addtocart", auth.verify, (req, res) => {


	let data = {
		userId: auth.decode(req.headers.authorization).id,
		products: req.body.products,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		totalAmount: req.body.totalAmount
	}

	userController.addToCart(data).then(resultFromController => res.send(resultFromController));

});


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController));

});



//route for turning a user into admin 
router.put("/:userId/adminify", auth.verify, (req,res) => {

	const userData= auth.decode(req.headers.authorization)
					 
	userController.userToAdmin(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
})


//route for getting orderhistory
router.get("/userorders",auth.verify,(req,res)=>{

	const userOrders=auth.decode(req.headers.authorization);
	userController.getOrders({id : userOrders.id}).then(resultFromController => res.send(resultFromController));

})
//
module.exports = router;